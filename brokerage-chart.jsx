import React from 'react'
import _ from 'lodash'
import moment from 'moment'
import { extendScale } from './utils'
import customPropTypes from 'sbtsbol-base/prop-types'
import { Tooltip } from './tooltip'
import { MarketChartForecast } from './forecast'
import style from './style.css'
import LoaderError from 'sbtsbol-loader-error'
import classnames from 'classnames'

const Y_MAX = 4
const RANGES = {}
export class BrokerageChart extends React.Component {
    static propTypes = {
        defaultDateRange: React.PropTypes.array,
        xMin: customPropTypes.moment,
        xMax: customPropTypes.moment,
        yMin: React.PropTypes.number,
        yMax: React.PropTypes.number,

        data: React.PropTypes.oneOfType([
            React.PropTypes.array,
            React.PropTypes.object
        ]),

        className: React.PropTypes.string,
        agreementId: React.PropTypes.string,
        children: React.PropTypes.node,

        isOpened: React.PropTypes.bool,
        forecasts: React.PropTypes.array,

        limitHoverLeft: React.PropTypes.number,

        agreementStatus: React.PropTypes.number,

        isLoading: React.PropTypes.bool,
        isDelayed: React.PropTypes.bool,

    }

    static defaultProps = {
        xMin: moment().subtract(30, 'days'),
        xMax: moment(),
        yMin: 0,
        yMax: Y_MAX,
        disabledAxisX: false,
        disabledAxisY: false,
        disabledTooltips: false,
        isOpened: false,
        limitHoverLeft: 62
    }

    constructor (props, context) {
        super(props, context)

        this.state = {
            xMin: props.xMin,
            xMax: props.xMax,
            yMin: props.yMin,
            yMax: props.yMax
        }

        this.setDomainSize(props.xMin, props.xMax)

        this.x = this.x.bind(this)
        this.y = this.y.bind(this)
        this.setRange = this.setRange.bind(this)
        this.onMouseMoveOverChart = this.onMouseMoveOverChart.bind(this)
        this.onMouseLeaveChart = this.onMouseLeaveChart.bind(this)
        this.fetchDataAgain = this.fetchDataAgain.bind(this)
    }

    onMouseMoveOverChart (e) {
        if (!this.props.isOpened) {
            return void 0
        }

        const { xMin, xMax } = this.props

        const hover = this.refs.hoverable.getBoundingClientRect()
        const xPxPosition = e.pageX - hover.left

        if (xPxPosition <= this.props.limitHoverLeft) {
            return this.onMouseLeaveChart()
        }

        const xPxWidth = hover.width
        const xDayWidth = xMax.diff(xMin, 'days') + 1
        const xDayPosition = Math.floor(xDayWidth * xPxPosition / xPxWidth)
        const tooltipPercentagePosition = 100 * xDayPosition / xDayWidth

        const xDate = xMin.clone().add(xDayPosition, 'days').toISODate()

        this.setState({
            isTooltipVisible: true,
            tooltipDate: xDate,
            tooltipPosition: tooltipPercentagePosition,
            tooltipWidth: 100 / xDayWidth
        })
    }

    onMouseLeaveChart () {
        this.setState({
            isTooltipVisible: false
        })
    }

    ranges = {}

    round (value, min) {
        const v = Math.abs(_.floor(value)).toString()
        if (v.length <= 2) {
            return value
        }
        min = Math.abs(min || value)
        const pow = Math.pow(10, v.length - 1)
        const num = _.floor(v / pow)

        const result = _.find([num * pow, (num + 0.2) * pow, (num + 0.5) * pow, (num + 1) * pow], (item) => {
            if (item > min) {
                return item
            }
        })

        return value < 0 ? result * -1 : result
    }

    setRange (range, id, agreementId="portfolio") {
        const type = this.props.type
        if (type) {
            if (!RANGES[agreementId]) {
                RANGES[agreementId] = {}
            }
            if (!RANGES[agreementId][this.props.type]) {
                RANGES[agreementId][this.props.type] = {}
            }
            RANGES[agreementId][this.props.type][id] = _.clone(range)
            range = _.reduce(RANGES[agreementId][this.props.type], extendScale, [0, 2])
        } else {
            if (!this.ranges[agreementId]) {
                this.ranges[agreementId] = {}
            }
            this.ranges[agreementId][id] = _.clone(range)
            range = _.reduce(this.ranges[agreementId][id], extendScale, [0, 2])
        }
        // Переделать расчет
        const { yMin, yMax } = this.getRangeWithOffsets(range)
        this.setState({
            yMin: this.round(yMin, range[0]),
            yMax: this.round(yMax, range[1])
        })
    }

    getRangeWithOffsets (range) {
        let yMin = range[0]
        let yMax = range[1]
        if (yMin >= 0 && yMax >= 0) {
            yMax = _.ceil(yMax * 1.2)
        } else if (yMin <= 0 && yMax <= 0) {
            yMin = _.floor(yMin * 1.2)
        } else if ((yMin * yMax) < 0) {
            yMax = _.ceil(yMax * 1.2)
            yMin = _.floor(yMin * 1.2)
        }
        return { yMin, yMax }
    }

    setDomainSize (fromValue, toValue) {
        this.domainSize = toValue.diff(fromValue, 'days') + 1
    }

    x (dateMoment, shiftToPrevDay = false) {
        let realWidth = 645
        if (this.refs.hoverable) {
            realWidth = this.refs.hoverable.offsetWidth
        }
        const daysDiff = dateMoment.clone().endOf('day').diff(this.props.xMin, 'days') + (shiftToPrevDay ? 0 : 1)
        return realWidth * daysDiff / this.domainSize
    }

    y (value = 0) {
        const realHeight = this.getCanvasHeight()
        const yDiff = this.state.yMax < value || this.state.yMin > value ? Y_MAX : this.state.yMax - value
        const range = Math.abs(this.state.yMin - this.state.yMax)

        return realHeight * yDiff / range
    }

    getCanvasHeight () {
        return (this.props.isOpened ? 180 : 120)
    }

    fetchDataAgain () {
        const { dateRange } = this.props
        this.props.onChangeDomain(dateRange, true)
    }

    render () {
        const { xMin, xMax, data, forecasts, agreementId } = this.props
        const chartChildrenProps = {
            calcX: this.x,
            calcY: this.y,
            setRange: this.setRange,
            xMin: xMin.clone().subtract(1, 'day').endOf('day'),
            xMax: xMax.clone().endOf('day'),
            yMin: this.state.yMin,
            yMax: this.state.yMax,
            round: this.round,
            agreementId
        }
        this.setDomainSize(this.props.xMin, this.props.xMax)

        const marketChartForecast = (this.props.agreementStatus !== 0) ? (this.props.forecasts &&
        <div className={style.forecast}>
            {!!this.props.forecasts.length &&
            <MarketChartForecast
                forecasts={forecasts}
                yMin={this.state.yMin}
                yMax={this.state.yMax}
                setRange={this.setRange}
                last={this.props.lastData}
                agreementId={this.props.agreementId}
            />}
        </div>) : null

        const { isLoading, isDelayed, errors, isOpened } = this.props
        const shouldLoaderBeShown = isOpened && (isLoading || isDelayed || errors)
        return (
            <div className={style.container}>
                { shouldLoaderBeShown && this.renderLoaderContainer() }
                <div className={style.canvas}>
                    <div
                        ref="hoverable"
                        onMouseMove={this.onMouseMoveOverChart}
                        onMouseLeave={this.onMouseLeaveChart}
                        className={`${style.wrapper} ${this.props.className}`}
                    >
                        <div className={style.stubForFF}>
                            <svg
                                className={style.chart}
                                ref="svg"
                                preserveAspectRatio="none"
                                height={this.getCanvasHeight() + 42} // 42 - padding offset
                                width={645}
                            >
                                {React.Children.map(this.props.children, (child) =>
                                    React.cloneElement(child,
                                        _.extend({}, { data }, child.props, chartChildrenProps)
                                    )
                                )}
                            </svg>
                        </div>
                        <div
                            className={style.tooltip}
                            style={{
                                display: this.state.isTooltipVisible ? 'block' : 'none',
                                left: `${this.state.tooltipPosition}%`,
                                width: `${this.state.tooltipWidth}%`
                            }}
                        >
                            {React.Children.map(this.props.children, (child, idx) =>
                                child.props.tooltipValue ?
                                    <Tooltip
                                        key={idx}
                                        id={child.props.id}
                                        value={child.props.tooltipValue}
                                        positionValue={child.props.tooltipPositionValue}
                                        title={child.props.tooltipTitle}
                                        date={child.props.tooltipDate}
                                        data={data}
                                        displayDate={this.state.tooltipDate}
                                        yMin={this.state.yMin}
                                        yMax={this.state.yMax}
                                        shouldDisplayPointer={child.props.tooltipShouldDisplayPointer}
                                    /> :
                                    null
                            )}
                        </div>
                    </div>
                </div>
                {marketChartForecast}
            </div>
        )
    }

    renderLoaderContainer () {
        const { isLoading, isDelayed, errors } = this.props
        return (
            <div className={classnames(style.loaderContainer, {
                [style.loaderContainerLoading]: isLoading,
                [style.loaderContainerDelayed]: isLoading && isDelayed,
                [style.loaderContainerErrors]: errors
            })}
            >
                <LoaderError
                    isLoading={isLoading}
                    isDelayed={isDelayed}
                    errors={errors}
                    theme={style}
                    onClick={this.fetchDataAgain}
                />
            </div>
        )
    }
}
