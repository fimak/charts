import _ from 'lodash'

/**
 * Extends scale (array of two numbers) with single value.
 * Returns new array, does not modify provided array
 *
 * @param  {array} scale  - ordered array of min and max value
 * @param  {number} value - value to extend given scale
 * @return {array}        - extended scale
 */
export function extendScaleWithValue (scale = [0, 1], value) {
    return [
        Math.min(scale[0], value),
        Math.max(scale[1], value)
    ]
}

/**
 * Extends scale (array of two numbers) with array of values,
 * for example, another scale.
 * Returns new array, does not modify provided arrays
 *
 * @param  {array} a - ordered array of min and max value
 * @param  {array} b - array of values to extend given scale
 * @return {array}   - extended scale
 */
export function extendScale (a, b) {
    if (_.isNumber(b)) {
        return extendScaleWithValue(a, b)
    }

    if (_.isArray(b)) {
        return _.reduce(b, extendScaleWithValue, a)
    }

    return a
}

export function getValueOrNull (point, path) {
    return _.has(point, path) ?
        _.get(point, path) :
        null
}
