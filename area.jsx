import _ from 'lodash'
import Graph from './graph'

export class Area extends Graph {
    d () {
        const data = _.toArray(this.props.data).slice().reverse()

        const xStart = this.props.calcX(this.props.x(_.first(data)))
        const yStart = this.props.calcY(this.props.y(_.first(data)))

        let last = { x: xStart, y: yStart }

        return _.reduce(
            _.tail(data),
            (d, point) => {
                const x = this.props.calcX(this.props.x(point))
                const y = this.props.calcY(this.props.y(point))
                const cx = last.x + (x - last.x) * 2 / 3

                d += `C${cx},${last.y} ${cx},${y} ${x},${y}`

                last = { x, y }

                return d
            },
            `M${xStart},${this.props.calcY(0)} V${yStart} `
        ) + `V${this.props.calcY(0)} z`
    }
}
