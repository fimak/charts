import React from 'react'
import _ from 'lodash'
import Graph from './graph'

export class Bar extends Graph {
    d () {
        const data = _.toArray(this.props.data)
        const xStart = this.props.calcX(this.props.x(_.first(data)), this.props.shiftToPrevDay)
        const yStart = this.props.calcY(this.props.y(_.first(data)))

        data.push({ dateMoment: _.last(data).dateMoment.clone().add(1, 'days') })

        return _.reduce(
            _.tail(data),
            (d, point) => {
                const x = this.props.calcX(this.props.x(point), this.props.shiftToPrevDay)
                const y = this.props.calcY(this.props.y(point))

                d += `H ${x} V ${y}`

                return d
            },
            `M${xStart},${this.props.calcY(0)} V${yStart}`
        ) + `V${this.props.calcY(0)} z`
    }
}
