import React from 'react'
import _ from 'lodash'
import classnames from 'classnames'
import FormattedNumber from 'sbtsbol-formattednumber'
import Time from 'sbtsbol-time'
import style from './style.css'
import moment from 'moment'
import { Msg } from 'sbtsbol-app'

export class Tooltip extends React.Component {
    static propTypes = {
        id: React.PropTypes.string.isRequired,
        value: React.PropTypes.func.isRequired,
        diff: React.PropTypes.func,
        last: React.PropTypes.object,
        positionValue: React.PropTypes.func.isRequired,
        date: React.PropTypes.func,
        data: React.PropTypes.object,
        title: React.PropTypes.string,
        displayDate: React.PropTypes.string,
        yMin: React.PropTypes.number.isRequired,
        yMax: React.PropTypes.number.isRequired,
        shouldDisplayPointer: React.PropTypes.func,
        displayLevel: React.PropTypes.bool,
        className: React.PropTypes.string
    }

    static defaultProps = {
        shouldDisplayPointer () {
            return 'center'
        },
        value () {},
        positionValue () {},
        date () {},
        diff () {}
    }

    constructor (props) {
        super(props)

        this.calcPercentageDistance = this.calcPercentageDistance.bind(this)
    }

    getTopPercentageOffset (point) {
        return 100 *
            (this.props.yMax - this.props.positionValue(point)) /
            (this.props.yMax - this.props.yMin)
    }

    getPointData () {
        return this.props.data[this.props.displayDate]
    }

    isPointNegative (point) {
        return this.props.value(point) < 0
    }

    calcPercentageDistance (value) {
        return 100 *
            value /
            (this.props.yMax - this.props.yMin)
    }

    render () {
        switch (this.props.id) {
            case 'total':
                return this.renderLarge()
            case 'x':
                return this.renderDateTooltip()
            default:
                return this.renderSmall()
        }
    }

    renderLarge () {
        const point = this.getPointData()

        if (!point) {
            return null
        }

        return (
            <div
                className={classnames(this.props.className, style.container, style[this.props.id], {
                    [style.level]: this.props.displayLevel
                })}
                style={{ top: `${this.getTopPercentageOffset(point)}%` }}
            >
                <div
                    className={classnames(
                        style.largeTooltip,
                        style[this.props.id]
                    )}
                >
                    {this.renderDate(point)}
                    {!!this.props.title && <Msg className={style.title} $={this.props.title} />}
                    {this.renderValue(point)}
                    {this.renderDiff(point)}
                    <svg
                        width="12px"
                        height="12px"
                        viewBox="0 0 12 12"
                        className={style.largePointer}
                    >
                        <path
                            className={classnames(
                                style.largePointerContent,
                                style[this.props.id]
                            )}
                            d="M3.5,0 L0,12 L12,4"
                        />
                    </svg>
                </div>
            </div>
        )
    }

    renderSmall () {
        const point = this.getPointData()

        if (!point) {
            return null
        }

        const pointer = this.props.shouldDisplayPointer(point, this.calcPercentageDistance)
        return (
            <div
                className={classnames(this.props.className, style.container, style[this.props.id], {
                    [style.level]: this.props.displayLevel
                })}
                style={{ top: `${this.getTopPercentageOffset(point)}%` }}
            >
                <div
                    className={classnames(
                        style.smallTooltip,
                        style[this.props.id],
                        style[pointer],
                        {
                            [style.negative]: this.isPointNegative(point)
                        }
                    )}
                >
                    {this.renderValue(point)}
                    {this.renderDiff(point)}
                    <svg
                        width="10px"
                        height="10px"
                        viewBox="0 0 10 10"
                        className={style.smallPointer}
                    >
                        {this.renderSmallPointer(pointer, this.isPointNegative(point))}
                    </svg>
                </div>
            </div>
        )
    }

    renderDateTooltip () {
        const point = this.getPointData()

        if (!point) {
            return null
        }

        return (
            <div
                className={classnames(style.container, style[this.props.id])}
            >
                <div className={style.dateTooltip}>
                    {this.renderText(point)}
                </div>
            </div>
        )
    }

    renderSmallPointer (pointer, isNegative) {
        switch (pointer) {
            case 'bottom': {
                return (
                    <path
                        className={classnames(
                            style.smallPointerContent,
                            style[this.props.id]
                        )}
                        d="M6,0.5 L10,0.5 L6,6.5"
                    />
                )
            }
            case 'top': {
                return (
                    <path
                        className={classnames(
                            style.smallPointerContent,
                            style[this.props.id]
                        )}
                        d="M6,9.5 L10,9.5 L6,3.5"
                    />
                )
            }
            default: {
                return (
                    <path
                        className={classnames(
                            style.smallPointerContent,
                            style[this.props.id],
                            {
                                [style.negative]: isNegative
                            }
                        )}
                        d="M6,1.5 L10,5 L6,8.5"
                    />
                )
            }
        }
    }

    renderValue (point) {
        const value = this.props.value(point)

        if (_.has(value, 'currency')) {
            return <FormattedNumber className={style.value} value={value.value} rounding="round" />
        }

        return <FormattedNumber className={style.value} value={value} rounding="round" decimals={this.props.id === 'quantity' ? 0 : 2} />
    }

    renderText (point) {
        const value = this.props.value(point)

        return value || ''
    }

    renderDiff (point) {
        const value = this.props.diff(point, this.props.last)

        if (_.isUndefined(value) || value === null) {
            return null
        }

        return (<FormattedNumber
            className={classnames(style.difference, {
                [style.positive]: value >= 0
            })}
            rounding="round"
            value={value}
        />)
    }

    renderDate (point) {
        const value = moment(this.props.date(point))

        return (
            <div className={style.date}>
                <Time value={value} format="D MMM" />
            </div>
        )
    }
}
