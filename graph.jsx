import React from 'react'
import _ from 'lodash'
import classnames from 'classnames'
import { extendScaleWithValue } from './utils'
import customPropTypes from 'sbtsbol-base/prop-types'
import style from './lines.css'

export default class Graph extends React.Component {
    static propTypes = {
        xMin: customPropTypes.moment,
        xMax: customPropTypes.moment,
        x: React.PropTypes.func,
        y: React.PropTypes.func,
        calcX: React.PropTypes.func,
        calcY: React.PropTypes.func,
        setRange: React.PropTypes.func,
        data: React.PropTypes.oneOfType([
            React.PropTypes.array,
            React.PropTypes.object]),
        name: React.PropTypes.string,
        className: React.PropTypes.string
    }

    static defaultProps = {
        x: () => 0,
        y: () => 0,
        setRange () {}
    }

    constructor (props) {
        super(props)
        this.state = {
            areaId: props.name || props.className || _.now('area')
        }
    }

    componentDidMount () {
        this.props.setRange(this.range(), this.state.areaId, this.props.agreementId)
    }

    componentWillReceiveProps (nextProps) {
        const { xMin, xMax } = nextProps
        if (!xMin.isSame(this.props.xMin) || !xMax.isSame(this.props.xMax) || (nextProps.y !== this.props.y)) {
            const countRange = this.range(nextProps)
            // if (!(countRange[0] === 0 && countRange[1] === 1)) {
                this.props.setRange(countRange, this.state.areaId, nextProps.agreementId)
            // }
        }
    }

    range (nextProps) {
        const { xMin, xMax, y } = nextProps || this.props
        return _.reduce(
            this.props.data,
            (memo, point) => {
                if (xMin.isBefore(point.dateMoment) && xMax.isSameOrAfter(point.dateMoment)) {
                    return extendScaleWithValue(memo, y(point))
                }
                return memo
            },
            [0, 1]
        )
    }

    d () {
        return 'M0,0 L0,0 Z'
    }

    render () {
        return React.createElement('path', { className: classnames(this.props.className, style.animate), d: this.d() })
    }
}
