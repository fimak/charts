import React, { PropTypes } from 'react'
import { Tooltip } from '../tooltip'
import customPropTypes from 'sbtsbol-base/prop-types'
import styles from './styles.css'

export class MarketChartForecastItem extends React.Component {
    static propTypes = {
        id: PropTypes.string.isRequired,
        value: React.PropTypes.func,
        positionValue: React.PropTypes.func,
        date: customPropTypes.moment,
        data: React.PropTypes.object,
        title: PropTypes.string,
        yMin: React.PropTypes.number,
        yMax: React.PropTypes.number,
        currentValue: PropTypes.number,
        shouldDisplayPointer: React.PropTypes.func,
        diff: React.PropTypes.func,
        last: React.PropTypes.object
    }
    
    constructor (props) {
        super(props)
        
        this.getDate = this.getDate.bind(this)
    }
    
    getDate () {
        return this.props.date
    }

    render () {
        return (
            <Tooltip
                id={this.props.id}
                value={this.props.value}
                positionValue={this.props.positionValue}
                date={this.getDate}
                data={this.props.data}
                title={this.props.title}
                displayDate="forecast"
                yMin={this.props.yMin}
                yMax={this.props.yMax}
                shouldDisplayPointer={this.props.shouldDisplayPointer}
                displayLevel
                className={styles.forecastItem}
                diff={this.props.diff}
                last={this.props.last}
            />
        )
    }
}
