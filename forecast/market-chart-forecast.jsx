import React from 'react'
import styles from './styles.css'
import _ from 'lodash'
import { MarketChartForecastItem } from './market-chart-forecast-item'
import { extendScale } from '../utils'

export class MarketChartForecast extends React.Component {
    static propTypes = {
        yMin: React.PropTypes.number,
        yMax: React.PropTypes.number,
        setRange: React.PropTypes.func,
        forecasts: React.PropTypes.array,
        last: React.PropTypes.object,
        shouldDisplayPointer: React.PropTypes.func,
        agreementId: React.PropTypes.string,
        children: React.PropTypes.node
    }

    static defaultProps = {}

    constructor (props) {
        super(props)

        this.setRange = this.setRange.bind(this)
    }

    componentWillMount () {
        this.setRange(this.props.forecasts)
    }

    componentWillReceiveProps (nextProps) {
        if (this.props.forecasts !== nextProps.forecasts) {
            this.setRange(nextProps.forecasts, nextProps.agreementId)
        }
    }

    setRange (forecasts, agreementId) {
        const range = _.reduce(forecasts,
            (memo, forecast) => extendScale(memo, forecast.positionValue(forecast.data.forecast)),
            [0, 5]
        )

        this.props.setRange(range, 'forecast', agreementId)
    }

    render () {
        if (_.isEmpty(this.props.forecasts)) {
            return null
        }
        return (
            <div className={styles.forecast}>
                {_.map(this.props.forecasts, (item) => (
                    <MarketChartForecastItem
                        key={item.id}
                        id={item.id}
                        value={item.value}
                        date={item.date}
                        positionValue={item.positionValue}
                        title={item.title}
                        yMin={this.props.yMin}
                        yMax={this.props.yMax}
                        data={item.data}
                        shouldDisplayPointer={item.shouldDisplayPointer}
                        diff={item.diff}
                        last={this.props.last}
                        currentValue={this.props.currentMoney ? this.props.currentMoney[item.id] : null}
                    />
                ))}
            </div>
        )
    }
}
