import _ from 'lodash'

function yAxis (range, middleMarks) {
    if (range[0] === Infinity || range[1] === - Infinity) {
        return null
    }

    let round = [
        (range[0].toFixed(0).length - 1) % 3 * 3,
        (range[1].toFixed(0).length - 1) % 3 * 3
    ]

    range[0] = _.floor(range[0], round[0])
    range[1] = _.ceil(range[1], round[1])

    let total = range[1] - range[0]
    let step = total / (middleMarks + 1)
    let marks = []
    for (let i = 1; i <= middleMarks; i++) {
        // if (range[0] + step * i >= 1) {
        marks.push(range[0] + step * i)
        // }
    }

    marks.push(range[1])

    return marks
}

export { yAxis }
