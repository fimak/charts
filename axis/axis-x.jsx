import React from 'react'
import moment from 'moment'
import _ from 'lodash'
import customPropTypes from 'sbtsbol-base/prop-types'
import { XAxisMarks } from './x-axis-marks'

import style from './axis.css'

export class ChartXAxis extends React.Component {
    static propTypes = {
        xMin: customPropTypes.moment,
        xMax: customPropTypes.moment,
        xMarksFrom: customPropTypes.moment,
        xMarksUntil: customPropTypes.moment
    }

    static defaultProps = {
        xMin: moment().subtract(1, 'month'),
        xMax: moment(),
        xMarksFrom: moment().subtract(5, 'years'),
        xMarksUntil: moment()
    }

    constructor (props) {
        super(props)

        this.gradientId = _.uniqueId('gradient')
        this.marks = this.generateMarks()
    }

    generateMarks () {
        const quantity = this.props.xMarksUntil.clone()
            .endOf('day')
            .add(1, 'day')
            .diff(this.props.xMarksFrom, 'days')

        const marks = []

        for (let i = 0; i < quantity; i++) {
            const day = moment(this.props.xMarksFrom.valueOf())
                .add(i + 1, 'days')

            marks[i] = {
                title: day.format('D MMM'),
                day: day.day(),
                distanceToWeekStart: Math.min(Math.abs(day.day() - 1), 8 - day.day()),
                distanceToMonthStart: Math.min(day.date() - 1, day.clone().endOf('month').date() + 1 - day.date())
            }
        }

        return marks
    }

    render () {
        const visibleLength = this.props.xMax.clone()
            .endOf('day')
            .diff(this.props.xMin, 'days')
        const step = 100 / visibleLength
        const visibleOffset = this.props.xMin
            .diff(this.props.xMarksFrom, 'days')

        return (
            <g className={style.x}>
                <defs>
                    <linearGradient id={this.gradientId}>
                        <stop offset="0%" stopColor="#fff" />
                        <stop offset="10%" stopColor="#fff" stopOpacity="0" />
                        <stop offset="97%" stopColor="#fff" stopOpacity="0" />
                        <stop offset="100%" stopColor="#fff" />
                    </linearGradient>
                </defs>
                <XAxisMarks
                    step={step}
                    offset={visibleOffset}
                    marks={this.marks}
                />
                <rect x="0" y="100%" width="100%" height="30" fill={`url(#${this.gradientId})`} />
            </g>
        )
    }
}
