import React from 'react'
import _ from 'lodash'

import style from './axis.css'

export class XAxisMarks extends React.Component {
    static propTypes = {
        step: React.PropTypes.number.isRequired,
        offset: React.PropTypes.number.isRequired,
        marks: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                title: React.PropTypes.string,
                day: React.PropTypes.number,
                distanceToWeekStart: React.PropTypes.number,
                distanceToMonthStart: React.PropTypes.number
            })
        ).isRequired
    }

    getMarkXPosition (position) {
        return (position - this.props.offset) * this.props.step
    }

    isVisible (mark) {
        // show Wednesday sometimes
        if (mark.day === 4 && mark.distanceToWeekStart * this.props.step > 10 && mark.distanceToMonthStart * this.props.step > 10) {
            return true
        }

        // show Monday when possible
        if (mark.distanceToWeekStart === 0 && mark.distanceToMonthStart * this.props.step > 10) {
            return true
        }

        // always show first day of month
        return mark.distanceToMonthStart === 0
    }

    isInside (position) {
        const calculatedPosition = this.getMarkXPosition(position)
        return calculatedPosition > -10 && calculatedPosition < 110
    }

    render () {
        return (
            <g>
                {_.map(this.props.marks, (mark, idx) => {
                    const x = this.getMarkXPosition(idx)
                    const visible = this.isVisible(mark)

                    if (this.isInside(idx)) {
                        return (
                            <g
                                key={`${mark.title}-${idx}`}
                                opacity={visible ? '1' : '0'}
                                className={style.xGroup}
                            >
                                <rect
                                    x={`${x}%`}
                                    y="100%"
                                    width="1"
                                    height="5"
                                    className={style.xMark}
                                />
                                <text
                                    x={`${x}%`}
                                    y="100%"
                                    dy="19"
                                    textAnchor="middle"
                                    className={style.xTitle}
                                >
                                    {mark.title}
                                </text>
                            </g>
                        )
                    }

                    return null
                })}
            </g>
        )
    }
}
