import React from 'react'
import _ from 'lodash'
import { shortenNumber } from 'sbtsbol-formattednumber'

import styleAxis from './axis.css'

export class ChartYAxis extends React.Component {
    static propTypes = {
        middleMarks: React.PropTypes.number,
        hasGrid: React.PropTypes.bool,
        yMin: React.PropTypes.number,
        yMax: React.PropTypes.number,
        calcY: React.PropTypes.func,
        round: React.PropTypes.func
    }

    static defaultProps = {
        yMin: 0,
        yMax: 1,
        middleMarks: 3,
        hasGrid: true
    }

    factoryStep () {
        const { yMin, yMax, middleMarks } = this.props

        let yMinMarks = 0
        let yMaxMarks = 0

        if (Math.abs(yMax * 100 / (yMax + Math.abs(yMin) - yMin * 100 / (yMin + Math.abs(yMax)))) < 20) {
            yMinMarks = _.floor(middleMarks / 2)
            yMaxMarks = _.floor(middleMarks / 2)
        } else if (yMax < Math.abs(yMin)) {
            yMinMarks = _.ceil(middleMarks / 2)
            yMaxMarks = _.floor(middleMarks / 2)
        } else {
            yMinMarks = _.floor(middleMarks / 2)
            yMaxMarks = _.ceil(middleMarks / 2)
        }

        return {
            yMinStep: Math.abs(yMin) / (yMinMarks + 1),
            yMaxStep: yMax / (yMaxMarks + 1),
            yMinMarks,
            yMaxMarks
        }
    }

    calculateStep (Y, marks, yStep) {
        const num = 1 * Math.pow(10, Math.abs(Math.round(Y)).toString().length - 2)
        const arrNum = _.map(Array(19), (i, key) => num * ((key + 1) * 0.5))
        return _(arrNum)
            .sortBy((o) => Math.abs(o - yStep))
            .first()
    }

    generateMarks () {
        const { yMin, yMax, calcY } = this.props

        const marks = {
            min: {
                value: yMin,
                position: calcY(yMin)
            },
            max: {
                value: yMax,
                position: calcY(yMax)
            }
        }

        const yMaxOrYMin = _.ceil(yMax >= Math.abs(yMin) ? yMax : Math.abs(yMin))
        const equalStep = yMaxOrYMin / (this.props.middleMarks + 1)

        if (yMin !== 0 && yMax !== 0 && this.props.middleMarks > 0) {
            marks.zero = {
                value: 0,
                position: calcY(0)
            }

            const { yMinStep, yMaxStep, yMinMarks, yMaxMarks } = this.factoryStep()


            const steps = {
                min: this.calculateStep(yMax, yMaxMarks, yMaxStep),
                max: this.calculateStep(yMin, yMinMarks, yMinStep)
            }

            let step = steps.min > steps.max ? steps.min : steps.max

            let p = step
            while ((p + p * 0.3) < yMax && step >= 1) {
                marks[p] = {
                    value: p,
                    position: calcY(p)
                }
                p += step
            }

            // if y-marks hoverring (distance is lower fontSize) there is condition to apply left shift to zero mark
            if (marks.zero && (Math.abs(marks.zero.position - marks.min.position) < 11 || Math.abs(marks.zero.position - marks.max.position) < 11)) {
                marks.zero.positionX = 7 // default 54
            }

            // step = this.calculateStep(yMin, yMinMarks, yMinStep)

            p = -step
            while ((p + p * 0.3) > yMin && step >= 1) {
                marks[p] = {
                    value: p,
                    position: calcY(p)
                }
                p -= step
            }
        } else if (this.props.middleMarks > 0 && equalStep >= 1) {

            let p = yMin + equalStep

            while (p < yMax && _.round(p) !== yMax) {
                marks[p] = {
                    value: p,
                    position: calcY(p)
                }
                p += equalStep
            }
        }


        return _(marks)
            .values()
            .sort((a, b) => a.value < b.value)
            .value()
    }

    render () {
        const marks = this.generateMarks()
        return (
            <g>
                <rect
                    x="0"
                    y="-40"
                    width="60"
                    height="190%"
                    className={styleAxis.yBackdrop}
                />
                {_.map(marks, (mark, idx) => (
                    <g key={idx}>
                        <text
                            x={_.get(mark, 'positionX', 54)}
                            y={mark.position}
                            textAnchor="end"
                            className={styleAxis.yTitle}
                        >
                            {shortenNumber(mark.value, {
                                shortenAt: 1000,
                                decimals: 0
                            })}
                        </text>
                        {this.props.hasGrid &&
                        <rect
                            x="60"
                            y={mark.position}
                            width="100%"
                            height="1"
                            className={styleAxis.yGridline}
                        />
                        }
                    </g>
                ))}
            </g>
        )
    }
}
