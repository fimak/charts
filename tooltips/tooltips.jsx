import React from 'react'
import _ from 'lodash'
import FormattedNumber from 'sbtsbol-formattednumber'
import Money from 'sbtsbol-money'
import Time from 'sbtsbol-time'
import style from './style.css'


export default class MarketTooltips extends React.Component {
    static propTypes = {

    }

    static defaultProps = {

    }

    constructor (props) {
        super(props)
        this.state = {
            tipVisible: false,
            tipIdx: 0
        }
    }

    onShowTooltip (e) {
        const tips = this.refs.tips.getBoundingClientRect()
        const position = e.pageX - tips.left
        let idx = Math.floor(this.props.axisDomain.data.length * (position / tips.width))
        idx = Math.max(Math.min(idx, this.props.axisDomain.data.length - 1), 0)

        this.setState({
            tipVisible: true,
            tipIdx: idx
        })
    }

    onHideTooltip () {
        this.setState({
            tipVisible: false
        })
    }

    renderTooltips (index) {
        if (!this.state.tipVisible) {
            return null
        }

        const value = this.props.axisDomain.data[index]
        const style = {
            width: 100 / this.props.axisDomain.data.length + '%',
            left: 100 * index / this.props.axisDomain.data.length + '%'
        }

        return (
            <div className={style.mark} style={style}>
                {_.map(this.props.tooltips, (tip, idx) => this.renderTooltip(value, tip, idx))}
            </div>
        )
    }

    renderTooltip (value, tip, idx) {
        let tipValue = 0
        try {
            tipValue = tip.value(value)
        } catch (e) {
            if (window.DEBUG) {
                console.error(e)
            }
        }

        let style = {
            height: 0,
            marginBottom: 0,
            bottom: 0
        }
        try {
            style = _.extend(style, tip.style(value, this.props.axisRange))
        } catch (e) {
            if (window.DEBUG) {
                console.error(e)
            }
        }

        let type = tip.type
        let asset = 'empty'
        let container = 'containersmall'
        let content = 'contentsmall'
        let pointer = 'pointersmall'
        let points = '0,60 30,35 30,85 0,60'
        let isLeft = tip.isLeft ? tip.isLeft(value, this.props.axisRange) : false

        if (['money', 'depo', 'free', 'blocked'].indexOf(type) < 0) {
            container = 'container'
            content = 'content'
            pointer = 'pointer'
            points = '0,120 60,0 120,70 0,120'
        }

        if (parseFloat(style.height) === 0 && tip.type) {
            return null
        }

        if (isLeft) {
            points = '120,60 90,35 90,85 120,60'
        }

        let tooltipStyle = {
            asset,
            container,
            content,
            pointer,
            points
        }

        return (
                <div key={idx} className={style.position} style={style}>
                    <div className={`${style[tooltipStyle.container]} ${style[tip.type]} ${isLeft ? style.left : ''}`}>
                        <div className={style[tooltipStyle.content]}>
                            <p className={style.date}><Time value={value.dateMoment} /></p>
                            <p className={style.title}>{tip.title}</p>
                            <p className={style.amount}>
                                {tip.isMoney ?
                                    <Money value={tipValue} showCents rounding="round" /> :
                                    <FormattedNumber value={tipValue} decimals rounding="round" upto={6} />
                                }
                            </p>
                        </div>
                        <svg
                            className={style[tooltipStyle.pointer]}
                            width="25px"
                            height="25px"
                            viewBox="0 0 120 120"
                        >
                            <polyline
                                points={`${tooltipStyle.points}`}
                                strokeWidth="1"
                            />
                        </svg>
                        <svg
                            className={style.pointersmallborder}
                            width="25px"
                            height="25px"
                            viewBox="0 0 120 120"
                        >
                            <polyline
                                className={style.polyline}
                                fill={style.pointerSmallBorder}
                                points={isLeft ? '120,60 60,10 60,110 120,60' : '0,60 60,10 60,110 0,60'}
                                strokeWidth="1" />
                        </svg>
                    </div>
                </div>
            )
    }

    render () {

        return (
            <div
                className={style.marks}
                style={style}
                onMouseMove={(e) => this.onShowTooltip(e)}
                onMouseLeave={(e) => this.onHideTooltip(e)}
                ref="tips"
            >
                {this.renderTooltips(this.state.tipIdx)}
            </div>
        )
    }
}
