import React from 'react'
import _ from 'lodash'
import classnames from 'classnames'
import Graph from './graph'
import style from './lines.css'

export class Line extends Graph {

    d () {
        const data = _.toArray(this.props.data).slice().reverse()
        const xStart = this.props.calcX(this.props.x(_.first(data)) || 0.00001) // 0.00001 for lineGradient
        const yStart = this.props.calcY(this.props.y(_.first(data)) || 0.00001)

        let last = { x: xStart, y: yStart }

        return _.reduce(
            _.tail(data),
            (d, point) => {
                const x = this.props.calcX(this.props.x(point))
                const y = this.props.calcY(this.props.y(point))

                const cx = last.x + (x - last.x) * 2 / 3

                d += `C${cx},${last.y} ${cx},${y} ${x},${y}`

                last.x = x
                last.y = y

                return d
            },
            `M${xStart},${yStart} `
        )
    }

    render () {
        const { gradient } = this.props
        const range = _.reduce(this.props.data, (memo, limit) => {
            const y = this.props.y(limit)
            if (_.isEmpty(memo)) {
                memo = [y, y]
            }
            return [
                (y < memo[0]) ? y : memo[0],
                (y > memo[1]) ? y : memo[1]
            ]
        }, {})

        let total = (range[1] + Math.abs(range[0]))
        if (total) {
            total = Math.floor(100 * Math.abs(range[0]) / total)
            total = (100 - (total - (total * 0.005))) // magic 0.005
        }
        total = total || 100

        const gradiented = gradient ? { stroke: `url(#${gradient.id})` } : null

        return (
            <g>
                {gradient && <defs>
                    <linearGradient id={gradient.id} x1="0%" y1="0%" x2="0%" y2="100%">
                        <stop offset={`${total}%`} stopColor={gradient.fill[0]} />
                        <stop offset={`${total}%`} stopColor={gradient.fill[1]} />
                    </linearGradient>
                </defs>}
                <path className={classnames(this.props.className, style.animate)} style={gradiented} d={this.d()} />
            </g>
        )
    }
}
