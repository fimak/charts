import moment from 'moment'

export const data = [
    {
        date: moment('2016-05-19'),
        money: 12345
    },
    {
        date: moment('2016-05-12'),
        money: 0
    },
    {
        date: moment('2016-05-05'),
        money: -1235
    },
    {
        date: moment('2016-04-28'),
        money: 1345
    },
    {
        date: moment('2016-04-27'),
        money: -1350
    },
    {
        date: moment('2016-04-26'),
        money: 12345
    },
    {
        date: moment('2016-04-25'),
        money: 1245
    },
    {
        date: moment('2016-04-22'),
        money: 1234
    },
    {
        date: moment('2016-04-21'),
        money: 345
    },
    {
        date: moment('2016-04-20'),
        money: 2345
    },
    {
        date: moment('2016-04-19'),
        money: 12345
    },
    {
        date: moment('2016-04-18'),
        money: 12345
    },
    {
        date: moment('2016-04-15'),
        money: 12345
    },
    {
        date: moment('2016-04-14'),
        money: 12345
    },
    {
        date: moment('2016-04-13'),
        money: 12345
    },
    {
        date: moment('2016-04-12'),
        money: 12345
    },
]
