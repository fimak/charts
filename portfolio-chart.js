import { BrokerageChart } from './brokerage-chart'

export class PortfolioChart extends BrokerageChart {
    getCanvasHeight () {
        return 120
    }
}
